package com.example.RoomDatabase;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import com.example.openweathermvvm.Model.City;

@Database(entities = City.class, version = 1)
public abstract class CityRoomDatabase extends RoomDatabase {

    public abstract CityDio cityDio();

    private static volatile CityRoomDatabase cityRoomDatabase;


    public static CityRoomDatabase getdatabase(final Context context) {

        if (cityRoomDatabase == null) {
            synchronized (CityRoomDatabase.class) {
                if (cityRoomDatabase == null) {
                    cityRoomDatabase = Room.databaseBuilder(context.getApplicationContext(),
                            CityRoomDatabase.class, "city_roomdatabase")
                            .build();
                }
            }
        }
        return cityRoomDatabase;
    }
}
