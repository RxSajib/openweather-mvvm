package com.example.RoomDatabase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.openweathermvvm.Model.City;

import java.util.List;

@Dao
public interface CityDio {

    @Insert
    public void insert(City city);


    @Query("SELECT * FROM cityDB")
    public LiveData<List<City>> getAllCity();

    @Delete
    public void DeleteData(City city);
}
