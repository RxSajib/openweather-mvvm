package com.example.openweathermvvm.Network.RoomNetwork;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.RoomDatabase.CityDio;
import com.example.RoomDatabase.CityRoomDatabase;
import com.example.openweathermvvm.Model.City;

import java.util.List;

public class RoomRepository extends AndroidViewModel {

    private CityDio cityDio;
    private CityRoomDatabase cityRoomDatabase;

    public RoomRepository(@NonNull Application application) {
        super(application);

        cityRoomDatabase = CityRoomDatabase.getdatabase(application);
        cityDio = cityRoomDatabase.cityDio();
    }

    public LiveData<List<City>> getAllCity(){
        return cityDio.getAllCity();
    }

    public void Insert(City city) {
        new InsertCityDatabase(cityDio).execute(city);
    }

    private class InsertCityDatabase extends AsyncTask<City, Void, Void>{

        private CityDio cityDio;

        public InsertCityDatabase(CityDio cityDio) {
            this.cityDio = cityDio;
        }

        @Override
        protected Void doInBackground(City... cities) {
            cityDio.insert(cities[0]);
            return null;
        }
    }

    public void RemoveData(City city){
        new RemoveDataAsyTask(cityDio).execute(city);
    }

    private final class RemoveDataAsyTask extends AsyncTask<City, Void, Void>{

        private CityDio cityDio;

        public RemoveDataAsyTask(CityDio cityDio) {
            this.cityDio = cityDio;
        }

        @Override
        protected Void doInBackground(City... cities) {
            cityDio.DeleteData(cities[0]);
            return null;
        }
    }
}
