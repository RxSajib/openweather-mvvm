package com.example.openweathermvvm.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.openweathermvvm.Api.Weekly;
import com.example.openweathermvvm.ApiClint.WeatherClint;
import com.example.openweathermvvm.Model.Response.WeeklyResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeeklyWeatherGET {

    private Application application;
    private MutableLiveData<WeeklyResponse> data;
    private Weekly weekly;


    public WeeklyWeatherGET(Application application){
        this.application = application;
        data = new MutableLiveData<>();
        weekly = new WeatherClint().getRetrofit().create(Weekly.class);
    }

    public LiveData<WeeklyResponse> getweekly_weatherdata(String Lat, String Log, String Exclude, String AppID, String Units){
        weekly.getweekly_weatherdata(Lat, Log, Exclude, AppID, Units).enqueue(new Callback<WeeklyResponse>() {
            @Override
            public void onResponse(@Nullable Call<WeeklyResponse> call,@Nullable Response<WeeklyResponse> response) {
                if(response.isSuccessful()){
                    data.setValue(response.body());
                }else {
                    Toast.makeText(application, String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                    data.setValue(null);
                }
            }

            @Override
            public void onFailure(@Nullable Call<WeeklyResponse> call,@Nullable Throwable t) {
                Toast.makeText(application, t.getMessage(), Toast.LENGTH_SHORT).show();
                data.setValue(null);
            }
        });
        return data;
    }
}
