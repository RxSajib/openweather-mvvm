package com.example.openweathermvvm.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.openweathermvvm.Api.WeatherCurrentHourly;
import com.example.openweathermvvm.ApiClint.WeatherClint;
import com.example.openweathermvvm.Model.Response.CurrentHourlyResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrentHourlyGET {

    private Application application;
    private MutableLiveData<CurrentHourlyResponse> data;
    private WeatherCurrentHourly currentHourly;

    public CurrentHourlyGET(Application application){
        this.application = application;
        data = new MutableLiveData<>();
        currentHourly = new WeatherClint().getRetrofit().create(WeatherCurrentHourly.class);
    }

    public LiveData<CurrentHourlyResponse> getcurrent_hourlydata(String Lat, String Log, String Exclude, String AppID, String Units){
        currentHourly.getcurrent_hourlydata(Lat, Log, Exclude, AppID, Units)
                .enqueue(new Callback<CurrentHourlyResponse>() {
                    @Override
                    public void onResponse(Call<CurrentHourlyResponse> call, Response<CurrentHourlyResponse> response) {
                        if(response.isSuccessful()){
                            data.setValue(response.body());
                        }else {
                            Toast.makeText(application, String.valueOf(response.body()), Toast.LENGTH_SHORT).show();
                            data.setValue(null);
                        }
                    }

                    @Override
                    public void onFailure(Call<CurrentHourlyResponse> call, Throwable t) {
                        Toast.makeText(application, t.getMessage(), Toast.LENGTH_SHORT).show();
                        data.setValue(null);


                    }
                });
        return data;
    }

}
