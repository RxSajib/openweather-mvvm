package com.example.openweathermvvm.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.openweathermvvm.Api.Weather;
import com.example.openweathermvvm.ApiClint.WeatherClint;
import com.example.openweathermvvm.Model.Response.CurrentWeatherResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrentWeatherGET {

    private Application application;
    private MutableLiveData<CurrentWeatherResponse> data;
    private Weather weather;

    public CurrentWeatherGET(Application application){
        this.application = application;
        data = new MutableLiveData<>();
        weather = new WeatherClint().getRetrofit().create(Weather.class);
    }

    public LiveData<CurrentWeatherResponse> getcurrentweather(String Location, String ApiKey, String Units){
         weather.getcurrent_weatherdata(Location, ApiKey, Units)
                .enqueue(new Callback<CurrentWeatherResponse>() {
                    @Override
                    public void onResponse(@Nullable Call<CurrentWeatherResponse> call,@Nullable Response<CurrentWeatherResponse> response) {
                        if(response.isSuccessful()){
                            data.setValue(response.body());
                        }
                        else {
                            Toast.makeText(application, String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                            data.setValue(null);
                        }
                    }

                    @Override
                    public void onFailure(@Nullable Call<CurrentWeatherResponse> call,@Nullable Throwable t) {
                        Toast.makeText(application, t.getMessage(), Toast.LENGTH_SHORT).show();
                        data.setValue(null);
                    }
                });
         return data;
    }
}
