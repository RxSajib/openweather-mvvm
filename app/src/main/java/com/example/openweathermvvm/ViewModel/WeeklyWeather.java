package com.example.openweathermvvm.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.openweathermvvm.Model.Response.WeeklyResponse;
import com.example.openweathermvvm.Network.WeeklyWeatherGET;

import org.jetbrains.annotations.NotNull;

public class WeeklyWeather extends AndroidViewModel {

    private WeeklyWeatherGET weeklyWeatherGET;

    public WeeklyWeather(@NonNull @NotNull Application application) {
        super(application);

        weeklyWeatherGET = new WeeklyWeatherGET(application);
    }

    public LiveData<WeeklyResponse>  getweeklyweather_data(String Lat, String Log, String Exclude, String AppID, String Units){
        return weeklyWeatherGET.getweekly_weatherdata(Lat, Log, Exclude, AppID, Units);
    }
}
