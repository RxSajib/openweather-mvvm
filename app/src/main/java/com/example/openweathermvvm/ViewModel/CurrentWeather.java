package com.example.openweathermvvm.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.openweathermvvm.Network.CurrentWeatherGET;
import com.example.openweathermvvm.Model.Response.CurrentWeatherResponse;

import org.jetbrains.annotations.NotNull;

public class CurrentWeather extends AndroidViewModel {

    private CurrentWeatherGET currentWeatherGET;

    public CurrentWeather(@NonNull @NotNull Application application) {
        super(application);

        currentWeatherGET = new CurrentWeatherGET(application);
    }

    public LiveData<CurrentWeatherResponse> getcurrent_weather(String Location, String AppID, String Units){
        return currentWeatherGET.getcurrentweather(Location, AppID, Units);
    }
}
