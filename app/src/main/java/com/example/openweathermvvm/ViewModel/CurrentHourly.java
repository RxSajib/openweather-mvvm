package com.example.openweathermvvm.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.openweathermvvm.Network.CurrentHourlyGET;
import com.example.openweathermvvm.Model.Response.CurrentHourlyResponse;

import org.jetbrains.annotations.NotNull;

public class CurrentHourly extends AndroidViewModel {

    private CurrentHourlyGET currentHourlyGET;

    public CurrentHourly(@NonNull @NotNull Application application) {
        super(application);
        currentHourlyGET = new CurrentHourlyGET(application);
    }

    public LiveData<CurrentHourlyResponse> getCurrentHourlydata(String Lat, String Log, String Exclude, String AppID, String Units){
        return currentHourlyGET.getcurrent_hourlydata(Lat, Log, Exclude, AppID, Units);
    }
}
