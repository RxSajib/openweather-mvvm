package com.example.openweathermvvm.UI.Fragement;

import android.content.res.Resources;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.openweathermvvm.Adapter.BookmarkAdapter;
import com.example.openweathermvvm.Model.City;
import com.example.openweathermvvm.Network.RoomNetwork.RoomRepository;
import com.example.openweathermvvm.R;
import com.example.openweathermvvm.databinding.BookmarkPageBinding;
import com.example.openweathermvvm.databinding.DeletebookmarkdialoagBinding;
import com.example.openweathermvvm.databinding.SearchcitylayoutBinding;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.List;
import java.util.UUID;


public class BookmarkPage extends Fragment {

    private BookmarkPageBinding binding;
    private RoomRepository roomRepository;
    private BookmarkAdapter bookmarkAdapter;


    public BookmarkPage() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.bookmark_page, container, false);

        init_view();
        return binding.getRoot();
    }

    private void init_view() {
        bookmarkAdapter = new BookmarkAdapter();
        binding.RecyclerView.setHasFixedSize(true);
        binding.RecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        binding.RecyclerView.setAdapter(bookmarkAdapter);
        roomRepository = new ViewModelProvider(this).get(RoomRepository.class);

        binding.SearchEdittextID.setPadding(30, getStatusBarHeight() + 40, 30, 0);

        binding.AddLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_searchdialoag();
            }
        });

        getweatherdata();
    }

    private int getStatusBarHeight() {
        int height;

        Resources myResources = getResources();
        int idStatusBarHeight = myResources.getIdentifier(
                "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = getResources().getDimensionPixelSize(idStatusBarHeight);

        } else {
            height = 0;

        }

        return height;
    }

    private void open_searchdialoag() {
        MaterialAlertDialogBuilder Mbuilder = new MaterialAlertDialogBuilder(getActivity());
        SearchcitylayoutBinding binding = DataBindingUtil.inflate(getActivity().getLayoutInflater(), R.layout.searchcitylayout, null, false);
        Mbuilder.setView(binding.getRoot());

        AlertDialog alertDialog = Mbuilder.create();
        alertDialog.show();

        binding.CitySearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.CityNameInput.getEditText().getText().toString().trim().isEmpty()) {
                    Toast.makeText(getActivity(), "Enter cityname", Toast.LENGTH_SHORT).show();
                } else {
                    City city = new City(UUID.randomUUID().toString(), binding.CityNameInput.getEditText().getText().toString().trim(), "Bangladesh");
                    roomRepository.Insert(city);

                    Toast.makeText(getActivity(), "City insert success", Toast.LENGTH_SHORT).show();
                    alertDialog.dismiss();
                }
            }
        });
    }

    private void getweatherdata() {
        roomRepository.getAllCity().observe(getViewLifecycleOwner()
                , new Observer<List<City>>() {
                    @Override
                    public void onChanged(List<City> cities) {
                        if (cities != null) {
                            bookmarkAdapter.setCityList(cities);
                            bookmarkAdapter.notifyDataSetChanged();

                            bookmarkAdapter.SetOnclickLisiner(new BookmarkAdapter.OnClick() {
                                @Override
                                public void OnClick(City city) {
                                    if (city != null) {
                                        BottomSheetDialog Mdialoag = new BottomSheetDialog(getActivity());
                                        DeletebookmarkdialoagBinding binding = DataBindingUtil.inflate(getActivity().getLayoutInflater(), R.layout.deletebookmarkdialoag, null, false);
                                        Mdialoag.setContentView(binding.getRoot());

                                        Mdialoag.show();

                                        binding.CancelButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                Mdialoag.dismiss();
                                            }
                                        });

                                        binding.DeleteButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                Mdialoag.dismiss();
                                                Delete_data(city);
                                            }
                                        });
                                    }
                                    else {
                                        binding.message.setVisibility(View.VISIBLE);
                                        Log.d("TAG", "NULL");
                                    }
                                }
                            });
                        }
                    }
                });
    }

    private void Delete_data(City city){
        roomRepository.RemoveData(city);

    }
}