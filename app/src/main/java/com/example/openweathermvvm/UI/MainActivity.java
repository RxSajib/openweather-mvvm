package com.example.openweathermvvm.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.example.openweathermvvm.R;
import com.example.openweathermvvm.UI.Fragement.BookmarkPage;
import com.example.openweathermvvm.UI.Fragement.HomePage;
import com.example.openweathermvvm.UI.Fragement.MenuPage;
import com.example.openweathermvvm.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);


        binding.HomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.HomeIcon.setImageDrawable(getResources().getDrawable(R.drawable.home_select));
                binding.WindeIcon.setImageDrawable(getResources().getDrawable(R.drawable.windunselected));
                binding.BookmarIcon.setImageDrawable(getResources().getDrawable(R.drawable.bookmarkunselected));
                binding.DashboardIcon.setImageDrawable(getResources().getDrawable(R.drawable.menuunselected));
                goto_homepage(binding, new HomePage());
            }
        });
        goto_homepage(binding, new HomePage());

        transparent_statusbar();
        binding.HomeIcon.setImageDrawable(getResources().getDrawable(R.drawable.home_select));
        binding.WindeIcon.setImageDrawable(getResources().getDrawable(R.drawable.windunselected));
        binding.BookmarIcon.setImageDrawable(getResources().getDrawable(R.drawable.bookmarkunselected));
        binding.DashboardIcon.setImageDrawable(getResources().getDrawable(R.drawable.menuunselected));


        binding.MenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.HomeIcon.setImageDrawable(getResources().getDrawable(R.drawable.homeunselected));
                binding.WindeIcon.setImageDrawable(getResources().getDrawable(R.drawable.windunselected));
                binding.BookmarIcon.setImageDrawable(getResources().getDrawable(R.drawable.bookmarkunselected));
                binding.DashboardIcon.setImageDrawable(getResources().getDrawable(R.drawable.menuselect));

                goto_menu(new MenuPage());
            }
        });


        binding.BookMatkButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.HomeIcon.setImageDrawable(getResources().getDrawable(R.drawable.homeunselected));
                binding.WindeIcon.setImageDrawable(getResources().getDrawable(R.drawable.windunselected));
                binding.BookmarIcon.setImageDrawable(getResources().getDrawable(R.drawable.likeselect));
                binding.DashboardIcon.setImageDrawable(getResources().getDrawable(R.drawable.menuselect));

                goto_bookmatkpage(new BookmarkPage());
            }
        });


    }

    private void goto_homepage(ActivityMainBinding binding, Fragment fragment) {
        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.MainFragement, fragment);
            transaction.commit();
        }
    }

    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    private void transparent_statusbar(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void goto_menu(Fragment fragment){
        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.MainFragement, fragment);
            transaction.commit();
        }
    }

    private void goto_bookmatkpage(Fragment fragment){
        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.MainFragement, fragment);
            transaction.commit();
        }
    }
}