package com.example.openweathermvvm.UI.Fragement;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.openweathermvvm.Data.DataManager;
import com.example.openweathermvvm.R;
import com.example.openweathermvvm.Model.Response.CurrentWeatherResponse;
import com.example.openweathermvvm.ViewModel.CurrentWeather;
import com.example.openweathermvvm.databinding.MenuPageBinding;


public class MenuPage extends Fragment {

    private CurrentWeather currentWeather;
    private MenuPageBinding menuBuilder;

    public MenuPage() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        menuBuilder = DataBindingUtil.inflate(inflater, R.layout.menu_page, container, false);

        init_view();
        return menuBuilder.getRoot();


    }

    private void init_view(){
        currentWeather = new ViewModelProvider(getActivity()).get(CurrentWeather.class);
        currentWeather.getcurrent_weather("Barishal", DataManager.APIKEY, "metric")
                .observe(getActivity(), new Observer<CurrentWeatherResponse>() {
                    @Override
                    public void onChanged(CurrentWeatherResponse currentWeatherResponse) {
                        if(currentWeatherResponse != null){
                            double temdouble = Double.valueOf(currentWeatherResponse.getMain().getTemp());
                            int temint = (int) temdouble;
                            menuBuilder.Tempruture.setText(String.valueOf(temint)+"°C");
                            menuBuilder.TemprutureText.setText(String.valueOf(temint)+"°C");
                            menuBuilder.WindSpeedText.setText(currentWeatherResponse.getWind().getSpeed()+"Km/h");
                            menuBuilder.WindSpeed.setText(currentWeatherResponse.getWind().getSpeed()+"Km/h");
                            menuBuilder.HuminityText.setText(currentWeatherResponse.getMain().getHumidity()+"%");
                            menuBuilder.GuestText.setText(String.valueOf(currentWeatherResponse.getClouds().getAll())+"%");
                            menuBuilder.WeatherStatus.setText(currentWeatherResponse.getWeatherList().get(0).getMain());
                            menuBuilder.Location.setText(currentWeatherResponse.getName());
                        }
                    }
                });
    }
}