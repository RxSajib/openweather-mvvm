package com.example.openweathermvvm.UI.Fragement;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.openweathermvvm.Adapter.DailyRecordAdapter;
import com.example.openweathermvvm.Adapter.WeeklyRecordAdapter;
import com.example.openweathermvvm.Data.DataManager;
import com.example.openweathermvvm.Model.Daily;
import com.example.openweathermvvm.R;
import com.example.openweathermvvm.Model.Response.CurrentHourlyResponse;
import com.example.openweathermvvm.Model.Response.WeeklyResponse;
import com.example.openweathermvvm.ViewModel.CurrentHourly;
import com.example.openweathermvvm.ViewModel.WeeklyWeather;
import com.example.openweathermvvm.databinding.HomePageBinding;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


public class HomePage extends Fragment {

    private CurrentHourly currentHourly;
    private HomePageBinding homePageBinding;
    private LinearLayoutManager linearLayoutManager;
    private DailyRecordAdapter adapter;
    private WeeklyWeather weeklyWeather;
    private WeeklyRecordAdapter weeklyRecordAdapter;
    private List<Daily> dailyList = new ArrayList<>();


    public HomePage() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        homePageBinding = DataBindingUtil.inflate(inflater, R.layout.home_page, container,false);

        //hourly



        inti_view();
        load_dataserver();

        return homePageBinding.getRoot();
    }

    private void inti_view(){
        homePageBinding.DayRecordView.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        homePageBinding.DayRecordView.setLayoutManager(linearLayoutManager);
        adapter = new DailyRecordAdapter();
        homePageBinding.DayRecordView.setAdapter(adapter);

        homePageBinding.DailyRecykerviewID.setHasFixedSize(true);
        homePageBinding.DailyRecykerviewID.setLayoutManager(new LinearLayoutManager(getActivity()));
        weeklyRecordAdapter = new WeeklyRecordAdapter();

        homePageBinding.DailyRecykerviewID.setAdapter(weeklyRecordAdapter);

        homePageBinding.MoreDetailsButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(DataManager.WebSite));
                startActivity(intent);

            }
        });
    }

    private void load_dataserver(){
        currentHourly = new ViewModelProvider(this).get(CurrentHourly.class);
        currentHourly.getCurrentHourlydata("36.3163", "74.6786", "daily", DataManager.APIKEY, "metric")
                .observe(getViewLifecycleOwner(), new Observer<CurrentHourlyResponse>() {
                    @Override
                    public void onChanged(CurrentHourlyResponse currentHourlyResponse) {
                        if(currentHourlyResponse != null){
                            homePageBinding.LocationName.setText("Barishal");
                            homePageBinding.Huminity.setText(String.valueOf(currentHourlyResponse.getCurrent().getHumidity())+"%");
                            homePageBinding.SpeedWind.setText(currentHourlyResponse.getCurrent().getWind_speed()+"Km/h");
                            homePageBinding.Visibility.setText(String.valueOf(currentHourlyResponse.getCurrent().getVisibility()/1000)+"Km");
                            homePageBinding.AirPrussure.setText(String.valueOf(currentHourlyResponse.getCurrent().getPressure())+"mPa");
                            homePageBinding.CloudVisibility.setText(String.valueOf(currentHourlyResponse.getCurrent().getClouds())+"%");
                            homePageBinding.HuminityTextV.setText(String.valueOf(currentHourlyResponse.getCurrent().getHumidity())+" %");
                            homePageBinding.WindeSpeedText.setText(String.valueOf(currentHourlyResponse.getCurrent().getWind_speed())+" m/h");

                            double doubleval = Double.valueOf(currentHourlyResponse.getCurrent().getTemp());
                            int intval = (int)doubleval;
                            homePageBinding.Tempruture.setText(String.valueOf(intval)+"°");
                            homePageBinding.BottomTem.setText(String.valueOf(intval)+"°C");


                            long SunRiseTimestamp = currentHourlyResponse.getSunrise();
                            Calendar calendar_sunrise = Calendar.getInstance(Locale.ENGLISH);
                            calendar_sunrise.setTimeInMillis(SunRiseTimestamp * 1000);
                            String SunRise = DateFormat.format(DataManager.TimeFormat, calendar_sunrise).toString();

                            long SunSetTimestamp = currentHourlyResponse.getSunset();
                            Calendar calendar_sunset = Calendar.getInstance(Locale.ENGLISH);
                            calendar_sunset.setTimeInMillis(SunSetTimestamp * 1000);
                            String SunSet = DateFormat.format(DataManager.TimeFormat, calendar_sunrise).toString();

                            homePageBinding.SunRise.setText(SunRise);
                            homePageBinding.Sunset.setText(SunSet);



                            adapter.setHourlyList(currentHourlyResponse.getHourlyList());
                            adapter.notifyDataSetChanged();

                        }
                    }
                });



        weeklyWeather = new ViewModelProvider(this).get(WeeklyWeather.class);
        weeklyWeather.getweeklyweather_data("36.3163", "74.6786", "hourly", DataManager.APIKEY, "metric")
                .observe(getViewLifecycleOwner(), new Observer<WeeklyResponse>() {
                    @Override
                    public void onChanged(WeeklyResponse weeklyResponse) {
                        if(weeklyResponse != null){
                            weeklyRecordAdapter.setDailyList(weeklyResponse.getDailyList());
                            weeklyRecordAdapter.notifyDataSetChanged();

                            Log.d("size", String.valueOf(weeklyResponse.getDailyList()));
                        }
                    }
                });
    }
}