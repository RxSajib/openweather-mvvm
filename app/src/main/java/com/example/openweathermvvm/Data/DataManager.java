package com.example.openweathermvvm.Data;

public class DataManager {

    public static final String BASE_URL = "https://api.openweathermap.org/data/2.5/";
    public static final String APIKEY = "7b533098cfa73b6fddf54d5d488b230e";
    public static final String DateFormat = "yyyy-MM-dd";
    public static final String TimeFormat = "HH:mm a";
    public static final String WebSite = "https://openweathermap.org/";

}
