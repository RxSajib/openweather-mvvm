package com.example.openweathermvvm.Adapter;

import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.openweathermvvm.Data.DataManager;
import com.example.openweathermvvm.Model.Hourly;
import com.example.openweathermvvm.R;
import com.example.openweathermvvm.ViewHolder.DayRecordViewHolder;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class DailyRecordAdapter extends RecyclerView.Adapter<DayRecordViewHolder> {

    private List<Hourly> hourlyList;

    public void setHourlyList(List<Hourly> hourlyList) {
        this.hourlyList = hourlyList;
    }

    @NonNull
    @NotNull
    @Override
    public DayRecordViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.day_weather_record, parent, false);
        return new DayRecordViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull DayRecordViewHolder holder, int position) {
        double Tempdouble = Double.valueOf(hourlyList.get(position).getTemp());
        int Tempint = (int) Tempdouble;
        holder.Temperature.setText(String.valueOf(Tempint)+"°C");

        long Timestamp = Long.parseLong(hourlyList.get(position).getTimestamp());
        Calendar calendar_time = Calendar.getInstance();
        calendar_time.setTimeInMillis(Timestamp * 1000);
        String Time = DateFormat.format(DataManager.TimeFormat, calendar_time).toString();

        holder.Time.setText(Time);


    }

    @Override
    public int getItemCount() {
        if(hourlyList == null){
            return 0;
        }else {
            return 7;
        }
    }
}
