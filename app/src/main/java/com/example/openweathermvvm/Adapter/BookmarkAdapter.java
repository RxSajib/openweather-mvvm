package com.example.openweathermvvm.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.openweathermvvm.Model.City;
import com.example.openweathermvvm.R;
import com.example.openweathermvvm.ViewHolder.BookMarkViewHolder;

import java.util.List;

public class BookmarkAdapter extends RecyclerView.Adapter<BookMarkViewHolder> {

    private List<City> cityList;
    private OnClick OnClick;

    public void setCityList(List<City> cityList) {
        this.cityList = cityList;
    }

    @NonNull
    @Override
    public BookMarkViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BookMarkViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.bookmark_iteam, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull BookMarkViewHolder holder, int position) {
        holder.CityName.setText(cityList.get(position).getLocationName());
        holder.CountryName.setText(cityList.get(position).getCountry());

        holder.Iteam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnClick.OnClick(cityList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        if(cityList == null){
            return 0;
        }else {
            return cityList.size();
        }
    }

    public interface OnClick{
        void OnClick(City city);
    }

    public void SetOnclickLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
