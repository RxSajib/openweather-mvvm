package com.example.openweathermvvm.Adapter;

import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.openweathermvvm.Model.Daily;
import com.example.openweathermvvm.R;
import com.example.openweathermvvm.ViewHolder.WeeklyRecordViewHolder;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class WeeklyRecordAdapter extends RecyclerView.Adapter<WeeklyRecordViewHolder> {

    private List<Daily> dailyList;

    public void setDailyList(List<Daily> dailyList) {
        this.dailyList = dailyList;
    }

    @NonNull
    @NotNull
    @Override
    public WeeklyRecordViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.daily_layout, parent, false);
        return new WeeklyRecordViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull WeeklyRecordViewHolder holder, int position) {

        long Timestamp = dailyList.get(position).getTimestamp();
        Calendar calendar_day = Calendar.getInstance(Locale.ENGLISH);
        calendar_day.setTimeInMillis(Timestamp * 1000);
        String Day = DateFormat.format("EEEE", calendar_day).toString();
        Log.d("DAY", Day);

        holder.Dayname.setText(Day);

        double MinTemperature = Double.valueOf(dailyList.get(position).getTemp().getMin());
        int MinTem = (int) MinTemperature;

        double MaxTemperature = Double.valueOf(dailyList.get(position).getTemp().getMax());
        int MaxTem = (int) MaxTemperature;

        holder.Temperature.setText(MinTem+"°C"+" - "+MaxTem+"°C");
    }

    @Override
    public int getItemCount() {
        if(dailyList == null){
            return 0;
        }
        else {
            return dailyList.size();
        }
    }
}
