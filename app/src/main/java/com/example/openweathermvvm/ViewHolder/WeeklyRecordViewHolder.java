package com.example.openweathermvvm.ViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.openweathermvvm.R;

import org.jetbrains.annotations.NotNull;

import carbon.widget.LinearLayout;

public class WeeklyRecordViewHolder extends RecyclerView.ViewHolder {

    public TextView Dayname, Temperature;
    public LinearLayout Image;

    public WeeklyRecordViewHolder(@NonNull @NotNull View itemView) {
        super(itemView);

        Dayname = itemView.findViewById(R.id.DayName);
        Temperature = itemView.findViewById(R.id.Tempruture);
        Image = itemView.findViewById(R.id.Icon);
    }
}
