package com.example.openweathermvvm.ViewHolder;

import android.media.Image;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.openweathermvvm.R;
import com.google.android.material.card.MaterialCardView;

public class BookMarkViewHolder extends RecyclerView.ViewHolder {

    public TextView Tempurature, CityName, CountryName;
    public ImageView ImageStatus;
    public TextView Humanity, WindSpeed;
    public MaterialCardView Iteam;

    public BookMarkViewHolder(@NonNull View itemView) {
        super(itemView);

        Tempurature = itemView.findViewById(R.id.TemprutureText);
        CityName = itemView.findViewById(R.id.CityName);
        CountryName = itemView.findViewById(R.id.CountryName);
        ImageStatus = itemView.findViewById(R.id.WeatherStatusImage);
        Humanity = itemView.findViewById(R.id.Humanity);
        WindSpeed = itemView.findViewById(R.id.WindSpeed);
        Iteam = itemView.findViewById(R.id.Iteam);
    }
}
