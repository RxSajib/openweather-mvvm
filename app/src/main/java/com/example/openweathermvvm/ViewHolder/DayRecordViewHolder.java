package com.example.openweathermvvm.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.openweathermvvm.R;

import org.jetbrains.annotations.NotNull;

import carbon.widget.LinearLayout;

public class DayRecordViewHolder extends RecyclerView.ViewHolder {

    public TextView Time, Temperature;
    public LinearLayout  image;

    public DayRecordViewHolder(@NonNull @NotNull View itemView) {
        super(itemView);

        Time = itemView.findViewById(R.id.Time);
        Temperature = itemView.findViewById(R.id.Tempruture);
        image = itemView.findViewById(R.id.Icon);
    }
}
