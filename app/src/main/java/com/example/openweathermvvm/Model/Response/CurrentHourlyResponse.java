package com.example.openweathermvvm.Model.Response;

import com.example.openweathermvvm.Model.Current;
import com.example.openweathermvvm.Model.Hourly;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CurrentHourlyResponse {

    @SerializedName("lat")
    private String lat;

    @SerializedName("lon")
    private String lon;

    @SerializedName("timezone")
    private String timezone;

    @SerializedName("current")
    private Current current;

    @SerializedName("sunrise")
    private long Sunrise;

    @SerializedName("sunset")
    private long Sunset;

    @SerializedName("clouds")
    private int clouds;

    @SerializedName("humidity")
    private int humidity;

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getClouds() {
        return clouds;
    }

    public void setClouds(int clouds) {
        this.clouds = clouds;
    }

    public long getSunrise() {
        return Sunrise;
    }

    public void setSunrise(long sunrise) {
        Sunrise = sunrise;
    }

    public long getSunset() {
        return Sunset;
    }

    public void setSunset(long sunset) {
        Sunset = sunset;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Current getCurrent() {
        return current;
    }

    public void setCurrent(Current current) {
        this.current = current;
    }


    @SerializedName("hourly")
    private List<Hourly> hourlyList;

    public List<Hourly> getHourlyList() {
        return hourlyList;
    }

    public void setHourlyList(List<Hourly> hourlyList) {
        this.hourlyList = hourlyList;
    }
}
