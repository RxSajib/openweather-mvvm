package com.example.openweathermvvm.Model;

import com.google.gson.annotations.SerializedName;

public class Daily {

    @SerializedName("dt")
    private long Timestamp;

    @SerializedName("sunrise")
    private long Sunrise;

    @SerializedName("sunset")
    private long Sunset;

    @SerializedName("clouds")
    private int clouds;

    @SerializedName("temp")
    private Temp temp;

    public Temp getTemp() {
        return temp;
    }

    public void setTemp(Temp temp) {
        this.temp = temp;
    }

    public long getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(long timestamp) {
        Timestamp = timestamp;
    }

    public long getSunrise() {
        return Sunrise;
    }

    public void setSunrise(long sunrise) {
        Sunrise = sunrise;
    }

    public long getSunset() {
        return Sunset;
    }

    public void setSunset(long sunset) {
        Sunset = sunset;
    }

    public int getClouds() {
        return clouds;
    }

    public void setClouds(int clouds) {
        this.clouds = clouds;
    }
}
