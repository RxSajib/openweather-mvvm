package com.example.openweathermvvm.Model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "cityDB")
public class City {

    @PrimaryKey()
    @NonNull
    private String ID;

    @ColumnInfo(name = "country_name")
    @NonNull
    private String Country;

    @ColumnInfo(name = "location_name")
    @NonNull
    private String LocationName;


    public City(){

    }

    public City(String ID, String country, String locationName) {
        this.ID = ID;
        Country = country;
        LocationName = locationName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getLocationName() {
        return LocationName;
    }

    public void setLocationName(String locationName) {
        LocationName = locationName;
    }
}
