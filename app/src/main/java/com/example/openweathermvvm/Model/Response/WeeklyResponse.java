package com.example.openweathermvvm.Model.Response;

import com.example.openweathermvvm.Model.Current;
import com.example.openweathermvvm.Model.Daily;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeeklyResponse {

    @SerializedName("lat")
    private String lat;

    @SerializedName("lon")
    private String lon;

    @SerializedName("timezone")
    private String timezone;


    @SerializedName("current")
    private Current current;


    @SerializedName("daily")
    private List<Daily> dailyList;

    public Current getCurrent() {
        return current;
    }

    public void setCurrent(Current current) {
        this.current = current;
    }

    public List<Daily> getDailyList() {
        return dailyList;
    }

    public void setDailyList(List<Daily> dailyList) {
        this.dailyList = dailyList;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
}
