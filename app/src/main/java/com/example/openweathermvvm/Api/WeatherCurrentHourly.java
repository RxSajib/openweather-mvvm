package com.example.openweathermvvm.Api;

import com.example.openweathermvvm.Model.Response.CurrentHourlyResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherCurrentHourly {

    @GET("onecall")
    Call<CurrentHourlyResponse> getcurrent_hourlydata(
            @Query("lat") String Lat,
            @Query("lon") String Log,
            @Query("exclude") String Exclude,
            @Query("appid") String AppID,
            @Query("units") String Units
    );

}
