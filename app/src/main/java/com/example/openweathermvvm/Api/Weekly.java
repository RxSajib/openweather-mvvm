package com.example.openweathermvvm.Api;

import com.example.openweathermvvm.Model.Response.WeeklyResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Weekly {

    @GET("onecall")
    Call<WeeklyResponse> getweekly_weatherdata(
            @Query("lat") String Lat,
            @Query("lon") String Log,
            @Query("exclude") String Exclude,
            @Query("appid") String AppID,
            @Query("units") String Units
    );
}
