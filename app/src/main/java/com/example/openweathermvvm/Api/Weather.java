package com.example.openweathermvvm.Api;

import com.example.openweathermvvm.Model.Response.CurrentWeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Weather {

    @GET("weather")
    Call<CurrentWeatherResponse> getcurrent_weatherdata(
            @Query("q") String LocationName,
            @Query("appid") String AppID,
            @Query("units") String metric
    );
}
